# rest api

## request
`POST /api/v1/analyse`

```json
{
  "id": "xxxxxx",
  "source": "pagerduty",
  "content": {
    "key": "homez/metrics-up/upp_core_realtime_remaining_event_delay.py",
    "topic": "Job metrics-up/upp_core_realtime_remaining_event_delay keeps failing",
    "details": [
      {
        "occurred_time": "2022-03-01T07:33:24Z",
        "text": "Success: Input size: 262985.931579; incoming data size check: upp-core-realtime-remaining-event number of records is 262985.931579 above threshold 60000\nFailure: Input size: 262985.931579; Delay check: upp-core-realtime-remaining-event processing delay time is 21344.5833333 above threshold 11049.4668068!\n "
      }
    ]
  },
  "team_id": "PCSKPWG",
  "occurred_time": "2022-03-01T07:33:24Z"
}
```

```json
{
  "source": "slack"
}
```

## response

```json
{
  "success": true,
  "errCode": "",
  "errMsg": "",
  "content": {
    "id": "xxxxxx",
    "solutions": [
      {
        "type": "text",
        "value": "",
        "score": 0.9999199800
      }
    ]
  }
}
```

```json```